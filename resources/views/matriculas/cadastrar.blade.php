@extends('layout.main')

@section('title')
  Cadastrar disciplinas
@endsection

@section('content')
  <form action="{{route('matriculas.store')}}" method="POST">
    @csrf
    <br>
    <label>Aluno</label>
    <select name="alunos_id" id="alunos_id">
      @foreach ($alunos as $alunos)
          <option value="{{ $alunos->id }}">{{ $alunos->name }}</option>
      @endforeach
    </select>
    <label>Disciplinas</label>
    <select name="disciplinas_id" id="disciplinas_id">
      @foreach ($disciplinas as $disciplinas)
          <option value="{{ $disciplinas->id }}">{{ $disciplinas->nome }}</option>
      @endforeach
    </select>
    <button type="submit">Enviar</button>
  </form>
@endsection
