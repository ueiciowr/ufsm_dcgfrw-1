<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <h3>Escolha o curso para lista as disciplinas</h3>
  <form action="{{ url('/relatorios/listadisciplinascurso') }}">
    <label for="cursos">Escolha o curso</label>
    <select name="id">
      @foreach ($cursos as $curso)
        <option value="{{ $curso->id }}">{{ $curso->nome }}</option>
      @endforeach
    </select>
    <button type="submit">Pesquisar</button>
  </form>
  <br>
  @if ($disciplinas_curso != null)
    <h2>Disciplinas deste curso</h2>
    <table>
      <tr>
        <th>Id</th>
        <th>Nome</th>
        <th>Carga horária</th>
      </tr>
      @foreach ($disciplinas_curso as $disciplina)
        <tr>
          <td>{{ $disciplina->id }}</td>
          <td>{{ $disciplina->nome }}</td>
          <td>{{ $disciplina->cargahoraria }}</td>
        </tr>
      @endforeach
    </table>
  @endif
</body>
</html>
