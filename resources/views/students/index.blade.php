<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Students</title>
</head>
<style>
  main {
    display: flex;
    flex-flow: column;
    width: 100%;
    min-height: 100vh;
    align-items: center;
    justify-content: center;
  }
</style>
<body>
  <main>
  <h2>Students</h2>
    <a href="{{url('students/create')}}">Adicionar Estudante</a>
    <section>
      
      <table border="1">
        <tr>
          <th>Nome</th>
          <th>Email</th>
          <th>Endereço</th>
          <th>Telefone</th>
          <th>CPF</th>
          <th>Editar</th>
          <th>Deletar</th>
        </tr>
        @foreach ($students as $student)
        <tr>
          <td style="padding: .4rem">{{ $student->name }}</td>
          <td style="padding: .4rem">{{ $student->email }}</td>
          <td style="padding: .4rem">{{ $student->address }}</td>
          <td style="padding: .4rem">{{ $student->phone }}</td>
          <td style="padding: .4rem">{{ $student->cpf }}</td>
          <td style="padding: .4rem">
            <a href="{{url('students/'.$student->id.'/edit')}}">
              <button>Editar</button>
            </a>
          </td>
          <td style="padding: .4rem"> 
            <form action="{{ route('students.destroy',$student->id) }}" method="POST">
              @csrf
              @method('DELETE')
              <button type="submit">Deletar</button>
            </form>
          </td>

        @endforeach
        </tr>
      </table>
    </section>
  </main>
</body>
</html>
