<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Editar Estudante</title>
</head>
<style>
body {
    display: flex;
    flex-flow: column;
    justify-content: center;
    align-items: center;
    min-height: 100vh;
    width: 100%;
}
form {
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  width: 20rem;
}
form div {
  display: flex;
  justify-content: space-evenly;
  width: 100%;
  align-items: center;
}
form div textarea {
  width: 87%;
  padding: 0.3rem 1rem;
}
form div input, textarea {
  margin: 1rem;
}
form button {
  padding: .7rem 2rem;
  margin-top: 2rem;
  cursor: pointer;
}
form div input {
  padding: .7rem 1rem;
}

</style>
<body>
  <h1>Editar Estudante</h1>
  <form action="{{ route('students.update', $student->id) }}" method="POST">
    @csrf
    @method('PUT')
    <div>
      <label>Nome do Estudante</label>
      <input name="name" type="text" value="{{ $student->name }}"  />
    </div>

    <div>
      <label>Email do Estudante</label>
      <input name="email" type="email" value="{{ $student->email }}"  />
    </div>

    <div>
      <label>Endereço do Estudante</label>
      <textarea name="address" type="text">{{ $student->address }}</textarea>
    </div>

    <div>
      <label>Telefone do Estudante</label>
      <input name="phone" type="number" value="{{ $student->phone }}"  />
    </div>

    <div>
      <label>CPF do Estudante</label>
      <input name="cpf" type="text" value="{{ $student->cpf }}" />
    </div>

    <button type="submit">Atualizar</button>
  </form>
</body>
</html>
