<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Editar Funcionario</title>
</head>
<style>
body {
    display: flex;
    flex-flow: column;
    justify-content: center;
    align-items: center;
    min-height: 100vh;
    width: 100%;
}
form {
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  width: 20rem;
}
form div {
  display: flex;
  justify-content: space-evenly;
  width: 100%;
  align-items: center;
}
form div textarea {
  width: 87%;
  padding: 0.3rem 1rem;
}
form div input, textarea {
  margin: 1rem;
}
form button {
  padding: .7rem 2rem;
  margin-top: 2rem;
  cursor: pointer;
}
form div input {
  padding: .7rem 1rem;
}

</style>
<body>
  <h1>Editar Funcionário {{ $funcionarios->id }}</h1>
  <form action="{{ route('funcionarios.update', $funcionarios->id) }}" method="POST">
    @csrf
    @method('PUT')
    <br>
    <label>Nome</label>
    <input type="text" name="nome" value="{{ $funcionarios->nome }}"  />

    <label>CPF</label>
    <input type="text" name="cpf" value="{{ $funcionarios->cpf }}" />

    <label>Email</label>
    <input type="text" name="email" value="{{ $funcionarios->email }}" />

    <label>Salário base</label>
    <input type="text" name="salario_base" value="{{ $funcionarios->salario_base }}" />

    <label>Ano de admissão</label>
    <input type="text" name="ano_admissao" value="{{ $funcionarios->ano_admissao }}" />

    <label>Cargos</label>
    <select name="cargos_id" id="cargos_id">
      @foreach ($cargos as $cargos)
          <option name="cargos_id" value="{{ $cargos->id }}">{{ $cargos->name }}</option>
      @endforeach
    </select>
    <button type="submit">Atualizar</button>
  </form>
</body>
</html>
