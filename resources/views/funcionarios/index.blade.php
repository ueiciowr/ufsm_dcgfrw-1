<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Funcionarios</title>
</head>
<style>
  main {
    display: flex;
    flex-flow: column;
    width: 100%;
    min-height: 100vh;
    align-items: center;
    justify-content: center;
  }
</style>
<body>
  <main>
  <h2>Funcionarios</h2>
    <a href="{{url('funcionarios/create')}}">Adicionar Funcionario</a>
    <section>
      <table border="1">
        <tr>
          <th>Nome</th>
          <th>CPF</th>
          <th>Email</th>
          <th>Salário Base</th>
          <th>Ano de Admissão</th>
          <th>Cargo</th>
          <th>Editar</th>
          <th>Deletar</th>
        </tr>
        @foreach ($funcionarios as $funcionarios)
        <tr>
          <td style="padding: .4rem">{{ $funcionarios->nome }}</td>
          <td style="padding: .4rem">{{ $funcionarios->cpf }}</td>
          <td style="padding: .4rem">{{ $funcionarios->email }}</td>
          <td style="padding: .4rem">{{ $funcionarios->salario_base }}</td>
          <td style="padding: .4rem">{{ $funcionarios->ano_admissao }}</td>
          <td style="padding: .4rem">{{ $funcionarios->cargos->name }}</td>
          <td style="padding: .4rem">
            <a href="{{url('funcionarios/'.$funcionarios->id.'/edit')}}">
              <button>Editar</button>
            </a>
          </td>
          <td style="padding: .4rem"> 
            <form action="{{ route('funcionarios.destroy',$funcionarios->id) }}" method="POST">
              @csrf
              @method('DELETE')
              <button type="submit">Deletar</button>
            </form>
          </td>
        @endforeach
        </tr>
      </table>
    </section>
  </main>
</body>
</html>
