@extends('layout.main')

@section('title')
  Cadastrar disciplinas
@endsection

@section('content')
  <form action="{{route('disciplinas.store')}}" method="POST">
    @csrf
    <br>
    <label>Nome da disciplina</label>
    <input type="text" name="nome"  />

    <label>Carga horária</label>
    <input type="text" name="cargahoraria"  />

    <label>Código curso</label>
    <select name="cursos_id" id="cursos_id">
      @foreach ($disciplinas as $disciplinas)
          <option name="cursos_id" value="{{ $disciplinas->id }}">{{ $disciplinas->nome }}</option>
      @endforeach
    </select>
    <button type="submit">Enviar</button>
  </form>
@endsection
