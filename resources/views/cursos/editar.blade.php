@extends('layout.main')

@section('title')
  Cadastrar cursos
@endsection

@section('content')
  <form action="{{route('cursos.update', $cursos->id)}}" method="POST">
    @csrf
    @method('PUT')
    <br>
    <label>Nome do curso</label>
    <input type="text" name="nome" value="{{$cursos->nome}}" />

    <label>Descricao do curso</label>
    <input type="text" name="descricao" value="{{$cursos->descricao}}" />

    <button type="submit">Alterar</button>
  </form>
@endsection
