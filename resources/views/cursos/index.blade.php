@extends('layout.main')

@section('title')
  Cursos Cadastrados
@endsection

@section('content')
  <a class="pt-2 d-block" href="{{url('cursos/create')}}">Adicionar curso</a>
  <br>
  @foreach($cursos as $curso)
    <hr class="mt-5 mb-5 d-block">
    <h3 >Nome: {{ $curso->nome }}</h3>
    <span>Id: {{ $curso->id }} </span>
    <p>Descrição: {{ $curso->descricao }}</p>
    <a class="d-block pt-2 pb-2" href="{{url('cursos/'.$curso->id.'/edit')}}">
      <button class="p-1 pl-3 pr-3 border-0 outline-0 bg-primary text-light">Editar</button>
    </a>
    <form action="{{ route('cursos.destroy',$curso->id) }}" method="POST">
      @csrf
      @method('DELETE')
      <button class="p-1 pl-3 pr-3 border-0 outline-0 bg-danger text-light" type="submit">Excluir</button>
    </form>
  @endforeach
@endsection
