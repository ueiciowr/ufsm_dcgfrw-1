@extends('layout.main')

@section('title')
  Cadastrar cursos
@endsection

@section('content')
  <form action="{{route('cursos.store')}}" method="POST">
    @csrf
    <br>
    <label>Nome do curso</label>
    <input type="text" name="nome" value="{{ old('nome') }}"  />

    <label>Descricao do curso</label>
    <input type="text" name="descricao" value="{{ old('descricao') }}" />

    <button type="submit">Enviar</button>
  </form>
  <br>
  @if ($errors->any())
    <div class="alert, alert-danger p-2">
      <strong>Não foi possível realizar cadastro, devido aos seguintes erros:</stro>
      <ul>
        @foreach ($errors->all() as $erro)
          <li>{{ $erro }}</li>
        @endforeach
      </ul>
    </div>
  @endif
@endsection
