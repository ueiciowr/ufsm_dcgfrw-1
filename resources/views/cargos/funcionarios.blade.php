<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<h1>Funcionarios</h1>
  <section>
      <table border="1">
        <tr>
          <th>Nome</th>
          <th>CPF</th>
          <th>Email</th>
          <th>Salário Base</th>
          <th>Ano de Admissão</th>
          <th>Cargo</th>
        </tr>
        @foreach ($funcionarios as $funcionarios)
        <tr>
          <td style="padding: .4rem">{{ $funcionarios->nome }}</td>
          <td style="padding: .4rem">{{ $funcionarios->cpf }}</td>
          <td style="padding: .4rem">{{ $funcionarios->email }}</td>
          <td style="padding: .4rem">{{ $funcionarios->salario_base }}</td>
          <td style="padding: .4rem">{{ $funcionarios->ano_admissao }}</td>
          <td style="padding: .4rem">{{ $funcionarios->cargos->name }}</td>
        @endforeach
        </tr>
      </table>
  </section>
</body>
</html>
