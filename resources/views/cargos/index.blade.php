<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Cargos</title>
</head>
<body>
  <h1>Cadastrar cargos</h1>
  <a href="{{url('cargos/create')}}">Adicionar Cargo</a>

  <h1>Cargos</h1>
  @foreach($cargos as $cargo)
  <form action="{{route('cargos.show', $cargo->id)}}" method="GET">
    @csrf
    @method('GET')
    <span name="cargos_id">{{ $cargo->name ?? '' }}</span>
    <button type="submit">Funcionários</button>
  </form>
  @endforeach
  <table border="1">
    <tr>
      <th>Nome</th>
      <th>Descrição</th>
      <th>Editar</th>
      <th>Deletar</th>
    </tr>
    @foreach ($cargos as $cargo)
    <tr>
      <td style="padding: .4rem">{{ $cargo->name }}</td>
      <td style="padding: .4rem">{{ $cargo->description }}</td>
      <td style="padding: .4rem">
        <a href="{{url('cargos/'.$cargo->id.'/edit')}}">
          <button>Editar</button>
        </a>
      </td>
      <td style="padding: .4rem"> 
        <form action="{{ route('cargos.destroy',$cargo->id) }}" method="POST">
          @csrf
          @method('DELETE')
          <button type="submit">Deletar</button>
        </form>
      </td>
    @endforeach
    </tr>
  </table>
</body>
</html>
