<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Editar Funcionario</title>
</head>
<style>
body {
    display: flex;
    flex-flow: column;
    justify-content: center;
    align-items: center;
    min-height: 100vh;
    width: 100%;
}
form {
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  width: 20rem;
}
form div {
  display: flex;
  justify-content: space-evenly;
  width: 100%;
  align-items: center;
}
form div textarea {
  width: 87%;
  padding: 0.3rem 1rem;
}
form div input, textarea {
  margin: 1rem;
}
form button {
  padding: .7rem 2rem;
  margin-top: 2rem;
  cursor: pointer;
}
form div input {
  padding: .7rem 1rem;
}

</style>
<body>
  <h1>Editar Cargo</h1>
  <form action="{{ route('cargos.update', $cargos->id) }}" method="POST">
    @csrf
    @method('PUT')
    <label>Nome</label>
    <input type="text" name="name" value="{{ $cargos->name }}"   />
    <label>Descrição</label>
    <input type="text" name="description" value="{{ $cargos->description }}"  />
    <button type="submit">Atualizar</button>
  </form>
</body>
</html>
