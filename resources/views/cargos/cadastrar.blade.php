<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <h1>Cadastrar cargos</h1>
  <form action="{{route('cargos.store')}}" method="POST">
    @csrf
    <br>
    <label>Nome</label>
    <input type="text" name="name"  />

    <label>Descrição</label>
    <input type="text" name="description"  />
    <button type="submit">Enviar</button>
  </form>
</body>
</html>
