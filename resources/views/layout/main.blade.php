<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <title>Document</title>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
      <a href="/" class="nav-item nav-link text-light">Home</a>
      <a href="/alunos" class="nav-item nav-link text-light">Alunos</a>
      <a href="/cursos" class="nav-item nav-link text-light">Cursos</a>
      <a href="/disciplinas" class="nav-item nav-link text-light">Disciplinas</a>
      <a href="/matriculas" class="nav-item nav-link text-light">Matrículas</a>
  </nav>
  <div class="container p-3">
    <div class="title">
      <h1 class="">@yield('title')</h1>
    </div>
    <section>
      @yield('content')
    </section>
  </div>
</body>
</html>
