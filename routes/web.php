<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AlunosController;
use App\Http\Controllers\CursosController;
use App\Http\Controllers\StudentsController;
use App\Http\Controllers\DisciplinasController;
use App\Http\Controllers\MatriculasController;
use App\Http\Controllers\FuncionariosController;
use App\Http\Controllers\CargosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function () {
    return view('hello');
});

Route::get('/alunos', [AlunosController::class, 'index']);
// Route::get('/disciplinas', [DisciplinasController::class, 'index']);

Route::resource('cursos', CursosController::class);

Route::resource('students', StudentsController::class);

// Aula relacionamentos de tabelas
Route::get('/relatorios/listadisciplinascurso', [CursosController::class, 'listaDisciplinasCurso']);

Route::resource('disciplinas', DisciplinasController::class);
Route::resource('matriculas', MatriculasController::class);


// Exercícios
Route::resource('funcionarios', FuncionariosController::class);
Route::resource('cargos', CargosController::class);