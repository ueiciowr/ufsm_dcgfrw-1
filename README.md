### How to use

<hr>

**Adicionar um .env na raíz para informar os dados de acesso ao database**

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=database_name
DB_USERNAME=database_user
DB_PASSWORD=database_password
```

**Criar as tabelas**

```
$ php artisan migrate
```

**Iniciar o servidor**

```
$ php artisan serve
```

**Outros comandos**

```
$ php artisan make:migration migration_name # criar migration
$ php artisan migrate # gerar tabelas
$ php artisan migrate:rollback # desfazer ultimo lote de migrations gerados
$ php artisan migrate:reset # reverter toda a base de dados
$ php artisan make:controller controller_name --resource # criar controller com todos os recursos
$ php artisan make:model model_name # criar model

$ php artisan migrate:rollback # rollback de todas as tabelas
```
