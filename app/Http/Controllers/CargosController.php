<?php

namespace App\Http\Controllers;

use App\Models\Cargos;
use App\Models\Funcionarios;
use Illuminate\Http\Request;

class CargosController extends Controller
{
    public function index()
    {
        $cargos = Cargos::all();
        return view('cargos/index', compact('cargos'));
    }

    public function create()
    {
        return view('cargos/cadastrar');
    }

    public function store(Request $request)
    {
        Cargos::create($request->all());
        return redirect('cargos');
    }

    public function show($id)
    {
        $funcionarios = Funcionarios::all()->where("cargos_id", "=", $id);
        return view('cargos/funcionarios', compact('funcionarios'));
    }

    public function edit($id)
    {
        $cargos = Cargos::find($id);
        return view('cargos/edit', compact('cargos'));
    }

    public function update(Request $request, $id)
    {
        $cargos = Cargos::find($id);
        echo($cargos);
        $cargos->update($request->all());
        return redirect('cargos');
    }

    public function destroy($id)
    {
        $cargos = Cargos::find($id);
        echo($cargos);
        $cargos->delete();
        return redirect('cargos');
    }
}