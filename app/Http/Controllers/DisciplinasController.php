<?php

namespace App\Http\Controllers;

use App\Models\Cursos;
use App\Models\Disciplinas;
use Illuminate\Http\Request;

class DisciplinasController extends Controller
{

    public function index()
    {
        $disciplinas = Disciplinas::all();
        return view('disciplinas/index', compact('disciplinas'));
    }


    public function create()
    {
        $disciplinas = Cursos::all();
        return view('disciplinas/cadastrar', compact('disciplinas'));
    }


    public function store(Request $request)
    {
        Disciplinas::create($request->all());
        return redirect('disciplinas');
    }

    public function show(Disciplinas $disciplinas)
    {
        //
    }

    public function edit(Disciplinas $disciplinas)
    {
        //
    }

    public function update(Request $request, Disciplinas $disciplinas)
    {
        //
    }

    public function destroy(Disciplinas $disciplinas)
    {
        //
    }
}
