<?php

namespace App\Http\Controllers;
use App\Models\Students;

class AlunosController extends Controller
{
    public function index() { 
        $alunos = Students::all();
        return view('alunos/index', compact('alunos'));
    }
}
