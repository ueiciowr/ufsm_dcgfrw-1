<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cursos;

class CursosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cursos = Cursos::all();
        return view('cursos/index', compact('cursos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // chama o formulário para cadastro
        return view('cursos/cadastrar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'nome' => 'required | min:5 | max:100',
            'descricao' => 'required',
        ];

        $params = [
            'required' => 'O preenchimento do campo :attribute é obrigatório',
            'max' => 'O campo :attribute possui tamanho máximo de :max caracteres',
            'min' => 'O campo :attribute possui tamanho máximo de :min caracteres'
        ];

        $request->validate($rules, $params);

        // cadastra o curso no db -> fillable
        Cursos::create($request->all());
        // redirect page
        return redirect('cursos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cursos = Cursos::find($id);
        return view('cursos/editar', compact('cursos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // buscar o registro a ser alterado
        $cursos = Cursos::find($id);
        $cursos->update($request->all());
        return redirect('cursos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // excluir do db
        $cursos = Cursos::find($id);
        $cursos->delete();
        return redirect('cursos');
    }
    
    public function listaDisciplinasCurso(Request $request) {
        $cursos = Cursos::all();
        $cursoEscolhido = Cursos::find($request->id);
        if(isset($cursoEscolhido->disciplinas_curso)){
            $disciplinas_curso = $cursoEscolhido->disciplinas_curso;
        } else {
            $disciplinas_curso = null;
        }

        return view('relatorios/listaDisciplinasCurso', compact('cursos', 'disciplinas_curso'));
    }
}