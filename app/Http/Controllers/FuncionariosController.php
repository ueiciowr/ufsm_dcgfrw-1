<?php

namespace App\Http\Controllers;

use App\Models\Cargos;
use App\Models\Funcionarios;
use Illuminate\Http\Request;

class FuncionariosController extends Controller
{

    public function index()
    {
        $funcionarios = Funcionarios::all();
        return view('funcionarios/index', compact('funcionarios'));
    }

    public function create()
    {
        $cargos = Cargos::all();
        return view('funcionarios/register', compact('cargos'));
    }

    public function store(Request $request)
    {
        Funcionarios::create($request->all());
        return redirect('funcionarios');
    }

    public function show(Funcionarios $funcionarios)
    {

    }

    public function edit($id)
    {
        $funcionarios = Funcionarios::find($id);
        $cargos = Cargos::all();
        return view('funcionarios/edit', compact('funcionarios', 'cargos'));
    }

    public function update(Request $request, $id)
    {
        $funcionarios = Funcionarios::find($id);
        $funcionarios->update($request->all());
        return redirect('funcionarios');
    }

    public function destroy($id)
    {
        $funcionarios = Funcionarios::find($id);
        $funcionarios->delete();
        return redirect('funcionarios');
    }
}