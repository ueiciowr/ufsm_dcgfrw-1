<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    protected $table = 'alunos';
    public $fillable = ['name', 'email', 'address', 'phone', 'cpf'];
    use HasFactory;
}
