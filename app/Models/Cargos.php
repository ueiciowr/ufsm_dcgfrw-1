<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Funcionarios;

class Cargos extends Model
{
    protected $table = 'cargos';
    protected $fillable = ['name', 'description'];

    public function funcionarios() {
        return $this->hasMany(Funcionarios::class, 'cargos_id'); 
    }

    use HasFactory;
}