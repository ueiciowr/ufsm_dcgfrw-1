<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Disciplinas;

class Cursos extends Model
{
    protected $table = 'cursos';
    public $fillable = ['nome', 'descricao'];
    use HasFactory;

    // Buscar as disciplinas que pertencem a esse cursos
    // Um para muitos
    public function disciplinas_curso() {
        return $this->hasMany(Disciplinas::class);
    }
}
