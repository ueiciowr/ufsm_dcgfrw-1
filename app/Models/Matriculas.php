<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Students;
use App\Models\Disciplinas;

class Matriculas extends Model
{
    protected $table = 'matriculas';
    protected $fillable = ['disciplinas_id', 'alunos_id'];

    public function disciplinas() {
        return $this->belongsTo(Disciplinas::class); // um para muitos reverso
    }
    public function alunos() {
        return $this->belongsTo(Students::class); // um para muitos reverso
    }
    use HasFactory;
}
