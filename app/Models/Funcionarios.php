<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Cargos;

class Funcionarios extends Model
{
    protected $table = 'funcionarios';
    public $fillable = ['id', 'nome', 'cpf', 'email', 'salario_base', 'ano_admissao', 'cargos_id'];
    use HasFactory;

    public function cargos() {
        return $this->belongsTo(Cargos::class);
    }
}