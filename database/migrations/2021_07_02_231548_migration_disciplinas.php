<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MigrationDisciplinas extends Migration
{
    public function up()
    {
        Schema::create('disciplinas', function (Blueprint $tabela) {
            $tabela->increments('id');
            $tabela->string('nome');
            $tabela->text('cargahoraria');
            // foreign
            $tabela->unsignedInteger('cursos_id');
            $tabela->foreign('cursos_id')->references('id')->on('cursos');
            $tabela->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('cursos');
    }
}
