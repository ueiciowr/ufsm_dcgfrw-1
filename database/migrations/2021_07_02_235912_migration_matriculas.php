<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MigrationMatriculas extends Migration
{
 public function up()
    {
        Schema::create('matriculas', function (Blueprint $tabela) {
            $tabela->increments('id');
            // foreign disciplinas many o many
            $tabela->unsignedInteger('disciplinas_id');
            $tabela->foreign('disciplinas_id')->references('id')->on('disciplinas');
            // foreign alunos
            $tabela->unsignedInteger('alunos_id');
            $tabela->foreign('alunos_id')->references('id')->on('alunos');
            $tabela->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('cursos');
    }
}
