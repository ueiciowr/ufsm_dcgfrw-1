<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MigrationFuncionarios extends Migration
{

    public function up()
    {
        Schema::create('funcionarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('cpf');
            $table->text('email');
            $table->string('salario_base');
            $table->string('ano_admissao');

            $table->unsignedInteger('cargos_id');
            $table->foreign('cargos_id')->references('id')->on('cargos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('funcionarios');
    }
}